{
  "components": {
    "schemas": {
      "IngestSheet": {
        "description": "An ingest sheet that kicks off an ingest sheet upload",
        "example": {
          "filename": "s3://bucket-name/project-folder/filename.csv",
          "id": "01DEMM44VWN8M3PJFY4J5JJJMF",
          "name": "Name of the Ingest Sheet",
          "project_id": "01DFBXCA303G43Y3695NMQDH8F"
        },
        "properties": {
          "filename": {
            "description": "Ingest Sheet Filename in S3 bucket",
            "type": "string"
          },
          "id": {
            "description": "Ingest Sheet ID",
            "format": "ulid",
            "readOnly": "true",
            "type": "string"
          },
          "inserted_at": {
            "description": "Creation timestamp",
            "format": "datetime",
            "type": "string"
          },
          "name": {
            "description": "Ingest Sheet Name",
            "maxLength": 140,
            "minLength": 4,
            "type": "string"
          },
          "project_id": {
            "description": "ID of the parent project",
            "format": "ulid",
            "type": "string"
          },
          "updated_at": {
            "description": "Update timestamp",
            "format": "datetime",
            "type": "string"
          }
        },
        "required": ["name", "project_id", "filename"],
        "title": "IngestSheet",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.IngestSheet"
      },
      "IngestSheetRequest": {
        "description": "POST body for creating an ingest sheet",
        "example": {
          "ingest_sheet": {
            "filename": "s3://bucket-name/project-folder/filename.csv",
            "name": "The Name of the Ingest Sheet",
            "project_id": "01DFBXPN3EEZSWC6QTCFYQHTBB"
          }
        },
        "properties": {
          "ingest_sheet": {
            "$ref": "#/components/schemas/IngestSheet"
          }
        },
        "required": ["ingest_sheet"],
        "title": "IngestSheetRequest",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.IngestSheetRequest"
      },
      "IngestSheetResponse": {
        "description": "Response schema for single ingest sheet",
        "example": {
          "filename": "s3://bucket-name/project-folder/filename.csv",
          "id": "01DEMM44VWN8M3PJFY4J5JJJMF",
          "name": "Name of the Ingest Sheet",
          "project_id": "01DFBXCA303G43Y3695NMQDH8F"
        },
        "properties": {
          "data": {
            "$ref": "#/components/schemas/IngestSheet"
          }
        },
        "title": "IngestSheetResponse",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.IngestSheetResponse"
      },
      "IngestSheetsResponse": {
        "description": "Response schema for multiple ingest sheets",
        "example": {
          "data": [
            {
              "filename": "s3://bucket-name/project-folder/filename.csv",
              "id": "01DEMM44VWN8M3PJFY4J5JJJMF",
              "name": "Name of the Ingest Sheet",
              "project_id": "01DFBXCA303G43Y3695NMQDH8F"
            },
            {
              "filename": "s3://bucket-name/project-folder/filename.csv",
              "id": "01DFBXN0BNB8YEPXCMCNGAWV2J",
              "name": "Another Name of the Ingest Sheet",
              "project_id": "01DFBXPN3EEZSWC6QTCFYQHTBB"
            }
          ]
        },
        "properties": {
          "data": {
            "description": "The ingest sheet details",
            "items": {
              "$ref": "#/components/schemas/IngestSheet"
            },
            "type": "array"
          }
        },
        "title": "IngestSheetsResponse",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.IngestSheetsResponse"
      },
      "PresignedUrl": {
        "description": "A presigned S3 url to use for upload",
        "example": {
          "url": "http://localhost:9001/dev-uploads/ingest_sheets/01DFENBFNJAMYKR3C9YT3NRVWZ.csv?contentType=binary%2Foctet-stream&x-amz-acl=public-read&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=minio%2F20190710%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190710T192152Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=a215cd85011af1cd54bxxxxx8ad67a1de83bf5b0f0yyyy4294c1bec6727a789"
        },
        "properties": {
          "url": {
            "description": "A presigned S3 url",
            "format": "uri",
            "type": "string"
          }
        },
        "required": ["url"],
        "title": "PresignedUrl",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.PresignedUrl"
      },
      "PresignedUrlResponse": {
        "description": "Response schema for presigned url to upload to S3",
        "example": {
          "url": "http://localhost:9001/dev-uploads/ingest_sheets/01DFENBFNJAMYKR3C9YT3NRVWZ.csv?contentType=binary%2Foctet-stream&x-amz-acl=public-read&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=minio%2F20190710%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190710T192152Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=a215cd85011af1cd54bxxxxx8ad67a1de83bf5b0f0yyyy4294c1bec6727a789"
        },
        "properties": {
          "data": {
            "$ref": "#/components/schemas/PresignedUrl"
          }
        },
        "title": "PresignedUrlResponse",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.PresignedUrlResponse"
      },
      "Project": {
        "description": "An Ingest Project",
        "example": {
          "folder": "sample-ingest-project-1561914496",
          "id": "01DEMM44VWN8M3PJFY4J5JJJMF",
          "title": "Sample Ingest Project"
        },
        "properties": {
          "folder": {
            "description": "s3 Folder",
            "readOnly": "true",
            "type": "string"
          },
          "id": {
            "description": "Project ID",
            "format": "ulid",
            "readOnly": "true",
            "type": "string"
          },
          "inserted_at": {
            "description": "Creation timestamp",
            "format": "datetime",
            "type": "string"
          },
          "title": {
            "description": "Project Title",
            "maxLength": 140,
            "minLength": 4,
            "type": "string"
          },
          "updated_at": {
            "description": "Update timestamp",
            "format": "datetime",
            "type": "string"
          }
        },
        "required": ["title"],
        "title": "Project",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.Project"
      },
      "ProjectRequest": {
        "description": "POST body for creating a project",
        "example": {
          "project": {
            "title": "The Name of the Project"
          }
        },
        "properties": {
          "project": {
            "$ref": "#/components/schemas/Project"
          }
        },
        "required": ["project"],
        "title": "ProjectRequest",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.ProjectRequest"
      },
      "ProjectResponse": {
        "description": "Response schema for single project",
        "example": {
          "data": {
            "folder": "sample-project-1561914427",
            "id": "01DEMM44VWN8M3PJFY4J5JJJMF",
            "title": "Sample Project"
          }
        },
        "properties": {
          "data": {
            "$ref": "#/components/schemas/Project"
          }
        },
        "title": "ProjectResponse",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.ProjectResponse"
      },
      "ProjectsResponse": {
        "description": "Response schema for multiple projects",
        "example": {
          "data": [
            {
              "folder": "sample-project-1561914427",
              "id": "01DEMM44VWN8M3PJFY4J5JJJMF",
              "title": "Sample Project"
            },
            {
              "folder": "another-sample-project-1561914398",
              "id": "01DEMHYCAS4XKY0685F40GF8XT",
              "title": "Another Sample Project"
            }
          ]
        },
        "properties": {
          "data": {
            "description": "The projects details",
            "items": {
              "$ref": "#/components/schemas/Project"
            },
            "type": "array"
          }
        },
        "title": "ProjectsResponse",
        "type": "object",
        "x-struct": "Elixir.MeadowWeb.Schemas.ProjectsResponse"
      }
    }
  },
  "info": {
    "title": "Meadow",
    "version": "1.0"
  },
  "openapi": "3.0.0",
  "paths": {
    "/api/v1/ingest_sheets": {
      "get": {
        "callbacks": {},
        "deprecated": false,
        "description": "List all ingest sheets",
        "operationId": "IngestSheetController.list_all_ingest_sheets",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/IngestSheetsResponse"
                }
              }
            },
            "description": "Project List Response"
          }
        },
        "summary": "List All Ingest Sheets",
        "tags": ["ingest_sheets"]
      }
    },
    "/api/v1/ingest_sheets/presigned_url": {
      "get": {
        "callbacks": {},
        "deprecated": false,
        "description": "Get presigned s3 url for upload",
        "operationId": "IngestSheetController.presigned_url",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PresignedUrlResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Get presigned s3 url for upload",
        "tags": ["ingest_sheets"]
      }
    },
    "/api/v1/projects": {
      "get": {
        "callbacks": {},
        "deprecated": false,
        "description": "List all projects",
        "operationId": "ProjectController.index",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectsResponse"
                }
              }
            },
            "description": "Project List Response"
          }
        },
        "summary": "List projects",
        "tags": ["projects"]
      },
      "post": {
        "callbacks": {},
        "deprecated": false,
        "description": "Create a project",
        "operationId": "ProjectController.create",
        "parameters": [],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ProjectRequest"
              }
            }
          },
          "description": "The project attributes",
          "required": true
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Create project",
        "tags": ["projects"]
      }
    },
    "/api/v1/projects/{id}": {
      "delete": {
        "callbacks": {},
        "deprecated": false,
        "description": "Delete a project by ID",
        "operationId": "ProjectController.delete",
        "parameters": [
          {
            "description": "Project ID",
            "example": "01DEHZZ8B9TNWZN7M1FXDP5NZB",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Delete project",
        "tags": ["projects"]
      },
      "get": {
        "callbacks": {},
        "deprecated": false,
        "description": "Show a project by ID",
        "operationId": "ProjectController.show",
        "parameters": [
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Show project",
        "tags": ["projects"]
      },
      "patch": {
        "callbacks": {},
        "deprecated": false,
        "description": "Update a project",
        "operationId": "ProjectController.update",
        "parameters": [
          {
            "description": "Project ID",
            "example": "01DEHZZ8B9TNWZN7M1FXDP5NZB",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ProjectRequest"
              }
            }
          },
          "description": "The project attributes",
          "required": true
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Update project",
        "tags": ["projects"]
      },
      "put": {
        "callbacks": {},
        "deprecated": false,
        "description": "Update a project",
        "operationId": "ProjectController.update",
        "parameters": [
          {
            "description": "Project ID",
            "example": "01DEHZZ8B9TNWZN7M1FXDP5NZB",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ProjectRequest"
              }
            }
          },
          "description": "The project attributes",
          "required": true
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Update project",
        "tags": ["projects"]
      }
    },
    "/api/v1/projects/{project_id}/ingest_sheets": {
      "get": {
        "callbacks": {},
        "deprecated": false,
        "description": "List all ingest sheets in a project",
        "operationId": "IngestSheetController.index",
        "parameters": [
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/IngestSheetsResponse"
                }
              }
            },
            "description": "Project List Response"
          }
        },
        "summary": "List Ingest Sheets in a project",
        "tags": ["ingest_sheets"]
      },
      "post": {
        "callbacks": {},
        "deprecated": false,
        "description": "Create an ingest sheet",
        "operationId": "IngestSheetController.create",
        "parameters": [
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/IngestSheetRequest"
              }
            }
          },
          "description": "The ingest sheet attributes",
          "required": true
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/IngestSheetResponse"
                }
              }
            },
            "description": "Ingest Sheet"
          }
        },
        "summary": "Create ingest sheet",
        "tags": ["ingest_sheets"]
      }
    },
    "/api/v1/projects/{project_id}/ingest_sheets/{id}": {
      "delete": {
        "callbacks": {},
        "deprecated": false,
        "description": "Delete an ingest sheet by ID",
        "operationId": "IngestSheetController.delete",
        "parameters": [
          {
            "description": "IngestSheet ID",
            "example": "01DEHZZ8B9TNWZN7M1FXDP5NZB",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/IngestSheetResponse"
                }
              }
            },
            "description": "IngestSheet"
          }
        },
        "summary": "Delete ingest sheet",
        "tags": ["ingest_sheets"]
      },
      "get": {
        "callbacks": {},
        "deprecated": false,
        "description": "Show a ingest sheet by ID",
        "operationId": "IngestSheetController.show",
        "parameters": [
          {
            "description": "Ingest Sheet ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ProjectResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Show ingest sheet",
        "tags": ["ingest_sheets"]
      },
      "patch": {
        "callbacks": {},
        "deprecated": false,
        "description": "Update an ingest sheet",
        "operationId": "IngestSheetController.update",
        "parameters": [
          {
            "description": "Ingest Sheet ID",
            "example": "01DEHZZ8B9TNWZN7M1FXDP5NZB",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/IngestSheetRequest"
              }
            }
          },
          "description": "The ingest sheet attributes",
          "required": true
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/IngestSheetResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Update an ingest sheet",
        "tags": ["ingest_sheets"]
      },
      "put": {
        "callbacks": {},
        "deprecated": false,
        "description": "Update an ingest sheet",
        "operationId": "IngestSheetController.update",
        "parameters": [
          {
            "description": "Ingest Sheet ID",
            "example": "01DEHZZ8B9TNWZN7M1FXDP5NZB",
            "in": "path",
            "name": "id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "description": "Project ID",
            "example": "01DEMRE5KZM6GCWVNGB0A2ZDN8",
            "in": "path",
            "name": "project_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/IngestSheetRequest"
              }
            }
          },
          "description": "The ingest sheet attributes",
          "required": true
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/IngestSheetResponse"
                }
              }
            },
            "description": "Project"
          }
        },
        "summary": "Update an ingest sheet",
        "tags": ["ingest_sheets"]
      }
    }
  },
  "security": [],
  "servers": [],
  "tags": []
}
